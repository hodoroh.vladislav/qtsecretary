#-------------------------------------------------
#
# Project created by QtCreator 2021-02-09T09:47:56
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = serveurmysql
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SRC_DIR = src
INCLUDE_DIR = $$SRC_DIR/*
FORMS_DIR = forms

VPATH += $$SRC_DIR

#TEMPLATE = app

INCLUDEPATH += src/App/include
INCLUDEPATH += src/Core/include
INCLUDEPATH += src/ui
INCLUDEPATH += /usr/include/mysql

#todo refactor, really scary sh*t
SOURCES += src/main.cpp
SOURCES += $$files("src/App/*.cpp", true)
SOURCES += $$files("src/Core/*.cpp", true)
HEADERS += $$files("src/App/*.h", true)
HEADERS += $$files("src/Core/*.h", true)
RESOURCES += $$files("resources/*.qrc", true)

FORMS += $$FORMS_DIR/*.ui

 message($$SOURCES)

win32:LIBS += lib/libmysql.dll
unix:LIBS += lib/libqsqlmysql.so

DESTDIR=bin
OBJECTS_DIR=obj
MOC_DIR=$$SRC_DIR/moc
UI_DIR=$$SRC_DIR/ui
QRC_DIR=$$SRC_DIR/qrc
