#include <QApplication>
#include <QMessageBox>
#include <qmessagebox.h>
#include <QFile>
#include <iostream>
#include <QDebug>

#include "App/include/MainWindow.h"

template<class T>
void loadStyle(T *app) {
    QFile stylesheet(":/style/style/stylesheet.css");
    if (!stylesheet.open(QFile::ReadOnly)) {
        qDebug() << "Stylesheet open error";
        stylesheet.close();
        return;
    }
    app->setStyleSheet(stylesheet.readAll());
    qDebug() << "Stylesheet applied";

    stylesheet.close();
}

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    loadStyle(&app);

    QFont font("Monospace");
    font.setStyleHint(QFont::TypeWriter);
    app.setFont(font);
    QCoreApplication::setAttribute(Qt::AA_UseStyleSheetPropagationInWidgetStyles, true);

    MainWindow w;  // Création de la fenêtre principale
    w.show();  // Affichage
    return app.exec();// exécution de l'application

}
