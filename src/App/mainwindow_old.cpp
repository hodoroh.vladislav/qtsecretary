//#include <QMessageBox>
//#include <qmessagebox.h>
//#include <QDebug>
//#include <QScrollBar>
//#include <QToolBar>
//#include <QSqlField>
//#include <QSqlRecord>
//#include <QSqlQuery>
//#include <utility>
//
//
//#include "include/mainwindow.h"
//#include "ui_mainwindow.h"
//#include "../Core/include/ConnectionPool.h"
//
//
//MainWindow::MainWindow(QWidget *parent) :
//        QMainWindow(parent),
//        ui(new Ui::MainWindow) {
//    ui->setupUi(this);
//
//    this->tableModel = nullptr;
//    this->table = this->ui->tableView;
//    this->stack = this->ui->stackW;
//    this->db = ConnectionPool::openConnection();
////    QMessageBox::critical(this, "Attention", "Pb d'accès", QMessageBox::Ok);
//    this->configure();
//    this->stack->setCurrentIndex(0);
//
//}
//
//MainWindow::~MainWindow() {
//    delete tableModel;
//    if (this->db.isOpen())
//        ConnectionPool::closeConnection(this->db);
//    delete ui;
//}
//
//
//void MainWindow::configureButton() {
//    qDebug() << "Click";
//    QPushButton *button = this->ui->spotAccessButton;
//    if (this->isSpotAvailable) {
//        button->setText("Place disponible");
//        button->setStyleSheet("background-color: rgb(159, 200, 124);color:#CBFC8F;");
//    } else {
//        button->setText("Place non disponible");
//        button->setStyleSheet("background-color: rgb(220, 52, 39);");
//    }
//}
//
//void MainWindow::paintEvent(QPaintEvent *) {
//    int w = ui->tableView->width() - ui->tableView->verticalHeader()->width()
//            - ui->tableView->verticalScrollBar()->width();
//    ui->tableView->setColumnWidth(1, w / 2);
//    ui->tableView->setColumnWidth(2, w / 2);
//}
//
//void MainWindow::UIHome() {
//
//    qDebug() << this->ui->homeTablesPlace->children();
//}
//
//
////void MainWindow::tableShow(QString table) {
////    if (tableModel)
////        delete tableModel;
////    tableModel = new QSqlTableModel(this, this->db);
////    tableModel->setView(table);
////    tableModel->insertColumns(5,10);
////    tableModel->setHeaderData(5,"Test",;
////    tableModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
//////    tableModel->setItemDelegate();
////    if (tableModel->select())
////        ui->tableView->setModel(tableModel);
////}
//
//void showParking() {
//
//}
//
//
//void MainWindow::tableShow(QString table) {
//    qDebug() << table << "\n";
//
//    if (this->tableModel) {
//        delete this->tableModel;
//        this->tableModel = nullptr;
//    }
//
//    QSqlDatabase connection = ConnectionPool::openConnection();
//
//    this->tableModel = new QSqlTableModel(this, connection);
//    this->tableModel->setTable(table);
//    this->tableModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
//    this->tableModel->select();
//
//    this->ui->tableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
//
//    this->ui->tableView->setModel(this->tableModel);
//
//    QVBoxLayout *l=new QVBoxLayout(this);
//
//    this->table->setContextMenuPolicy(Qt::CustomContextMenu);
//    connect(this->table, SIGNAL(customContextMenuRequested(QPoint)),
//            SLOT(customMenuRequested(QPoint)));
//
//    this->table->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
//    connect(this->table->horizontalHeader(),
//            SIGNAL(customContextMenuRequested(QPoint)),
//            SLOT(customHeaderMenuRequested(QPoint)));
////    this->ui->tableView->addWidget(this->table);
//
//    this->ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
//
//    l->addWidget(this->table);
////    this->uiIsAvailablePlace();
//
////    this->ui->tableView->hideColumn(0);
//    this->ui->tableView->show();
//
//    ConnectionPool::closeConnection(connection);
//    qDebug() << (tableModel->rowCount());
//
//
//}
//
//void MainWindow::customMenuRequested(QPoint pos) {
//    QModelIndex index = table->indexAt(pos);
//    qDebug() << index ;
//
//    QMenu *menu = new QMenu(this);
//    menu->addAction(new QAction("Modifier", this));
//    menu->addAction(new QAction("Supprimer", this));
//    menu->addAction(new QAction("Action 3", this));
//    menu->popup(table->viewport()->mapToGlobal(pos));
//}
//
//void MainWindow::customHeaderMenuRequested(QPoint pos) {
//    int column = table->horizontalHeader()->logicalIndexAt(pos);
//    qDebug() << column ;
//    QMenu *menu = new QMenu(this);
//    menu->addAction(new QAction("Header Action 1", this));
//    menu->addAction(new QAction("Header Action 2", this));
//    menu->addAction(new QAction("Header Action 3", this));
//    menu->popup(table->horizontalHeader()->viewport()->mapToGlobal(pos));
//}
//
//void MainWindow::uiIsAvailablePlace() {
//
//    for (int i = 0; i < this->tableModel->rowCount(); i++) {
//        QPushButton *btn = new QPushButton();
//
//        switch (this->tableModel->record(i).value("libre").toInt()) {
//            case true:
//                btn->setText("Libre");
//                btn->setProperty("cssClass", "btn_success");
//                break;
//
//            case false:
//                btn->setText("Occupé");
//                btn->setProperty("cssClass", "btn_danger");
//                break;
//        }
//        this->ui->tableView->setIndexWidget(this->ui->tableView->model()->index(i, 1), btn);
//
//    }
//
//
//}
//
//
//void MainWindow::on_tableView_doubleClicked(const QModelIndex &index) {
//    QSqlRecord sqlRecord = tableModel->record(index.row());
//    int id = sqlRecord.field("id_place").value().toInt();
//    QString libre = sqlRecord.field("libre").value().toString();
//    QString id_place = sqlRecord.field("id_place").value().toString();
//    QString str = "ligne = " + (QString::number(index.row())) + " \n libre = " + libre + " \n id = " + id_place;
//
//    QMessageBox::information(this, "Sélection", str, QMessageBox::Ok);
//}
//
//
//void MainWindow::on_actionSupprimer_triggered() {
//    if (QMessageBox::question(this, "Supression", "?") != QMessageBox::Yes)
//        return;
//
//    int ligne = ui->tableView->currentIndex().row();
//    if (ligne < 0) return;
//
//    QSqlRecord sqlRecord = tableModel->record(ligne);
//
//    int id = sqlRecord.field("id_place").value().toInt();
//
////    QSqlQuery query(db);
//
////    if (!query.exec("DELETE FROM place WHERE id_place=" + QString::number(id)))
////        QMessageBox::critical(this, "Attention", "Pb Req", QMessageBox::Ok);
//
////    on_actionRafraichir_triggered();
//
//}
//
//
//void MainWindow::on_actionModifier_triggered() {
//
//    if (QMessageBox::question(this, "Modification", "?") != QMessageBox::Yes)
//        return;
//
//    int ligne = ui->tableView->currentIndex().row();
//
//    if (ligne < 0) return;
//
//    QSqlRecord sqlRecord = tableModel->record(ligne);
//
//    int id = sqlRecord.field("id_place").value().toInt();
//    QString libre = sqlRecord.field("libre").value().toString();
//    QString numero = sqlRecord.field("numero").value().toString();
//
//
////    QSqlQuery query(db);
////
////    if (!query.exec(
////            "UPDATE place SET libre='" + libre + "',numero='" + numero + "' WHERE id_place=" + QString::number(id)))
//
//
//    QMessageBox::critical(this, "Attention", "Pb Req", QMessageBox::Ok);
//
////    on_actionRafraichir_triggered();
//
//}
//
//
//void MainWindow::on_actionInsertion_triggered() {
////    QString numero = ui->stackedWidget->lineEdit_3->text();
////    QSqlQuery query(db);
////    qDebug() << query.exec("INSERT INTO place (libre,numero) VALUES ('" + libre + "','" + numero + "');");
//
////    on_actionRafraichir_triggered();
//
//}
//
//
//void MainWindow::on_pushButton_clicked() {
//    this->isSpotAvailable = !this->isSpotAvailable;
//
//    this->configureButton();
//}
//
//
//void MainWindow::on_homeButton_clicked() {
//    this->ui->stackW->setCurrentIndex(0);
//    this->UIHome();
//}
//
//
//void MainWindow::on_tableChoices_clicked() {
//    auto *clickedButton = qobject_cast<QPushButton *>(sender());
//    if (clickedButton) {
//        QString clickedBuilding = clickedButton->text();
//        QString clickedButtonName = clickedButton->objectName();
//
//        qDebug() << clickedButton << " " << clickedButtonName;
//    }
//
//    this->stack->setCurrentIndex(1);
//
//    this->configureButton();
//}
//
//
//void MainWindow::on_spotAccessButton_clicked() {
//    this->configureButton();
//    this->isSpotAvailable = !this->isSpotAvailable;
//}
//
//void MainWindow::on_insert_button_clicked() {
//    qDebug() << "Click";
//}
//
//void MainWindow::configure() {
//
//    connect(this->ui->personnelButton, &QPushButton::clicked, this, [=]() {
//        onTableSelection("personnel");
//    });
//    connect(this->ui->parkingButton, &QPushButton::clicked, this, [=]() {
//        onTableSelection("place");
//    });
//    connect(this->ui->enregistrementButton, &QPushButton::clicked, this, [=]() {
//        onTableSelection("enregistrement");
//    });
//}
//
//void MainWindow::onTableSelection(QString table) {
//
//    qDebug() << "table selected ";
//    this->stack->setCurrentIndex(1);
//    this->tableShow(std::move(table));
//}
//
//
