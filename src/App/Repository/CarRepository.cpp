

#include <QSqlDatabase>
#include "include/CarRepository.h"
#include "../../Core/include/ConnectionPool.h"
#include "../Entity/inlcude/CarEntity.h"

QString CarRepository::getDefaultUpdateQuery(){
    return "update vehicule set immatriculation = ?, type = ?, couleur = ? where id = ?";
}

QString CarRepository::getDefaultInsertQuery(){
    return "insert into vehicule (immatriculation, type, couleur) values (?, ?, ?)";
}

void CarRepository::tdelete(QString rowIndex) {
    BaseRepository::tdelete("vehicule", rowIndex);
}

void CarRepository::tupdate(QMap<QString, QString> data, QString id) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);
    query.prepare(getDefaultUpdateQuery());
    query.addBindValue(data["immatr"]);
    query.addBindValue(data["type"]);
    query.addBindValue(data["color"]);
    query.addBindValue(id);
    ConnectionPool::closeConnection(connection);
}

void CarRepository::tinsert(QMap<QString, QString> data) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);

    query.prepare(getDefaultInsertQuery());
    query.addBindValue(data["immatr"]);
    query.addBindValue(data["type"]);
    query.addBindValue(data["color"]);

    ConnectionPool::closeConnection(connection);
}

void CarRepository::tupdate( Entity &entity) {

    CarEntity &e = dynamic_cast<CarEntity &>(entity);
    QSqlDatabase connection = ConnectionPool::openConnection();

    QSqlQuery query(connection);
    query.prepare(getDefaultUpdateQuery());
    query.addBindValue(e.immatr);
    query.addBindValue(e.type);
    query.addBindValue(e.color);
    query.addBindValue(e.id);

    if (!query.exec()) {
        qDebug() << "Update enity car failed";
//        qDebug() << "Update enity " << entity.eName() << " failed";
    }
    ConnectionPool::closeConnection(connection);
}

void CarRepository::tinsert( Entity &entity) {

    CarEntity &e = dynamic_cast<CarEntity &>(entity);
    QSqlDatabase connection = ConnectionPool::openConnection();

    QSqlQuery query(connection);
    query.prepare(getDefaultInsertQuery());
    query.addBindValue(e.immatr);
    query.addBindValue(e.type);
    query.addBindValue(e.color);
    if (!query.exec()) {
        qDebug() << "insert enity car failed";
//        qDebug() << "Update enity " << entity.eName() << " failed";
    }
    ConnectionPool::closeConnection(connection);
}

QSqlQuery* CarRepository::tselect() {
    return BaseRepository::tselect("vehicule");
}

