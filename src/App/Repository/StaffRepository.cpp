#include "include/StaffRepository.h"
#include "../../Core/include/ConnectionPool.h"
#include "../Entity/inlcude/StaffEntity.h"
#include <QDebug>

QString StaffRepository::getDefaultUpdateQuery() {
//    UPDATE table
//    SET colonne_1 = 'valeur 1', colonne_2 = 'valeur 2', colonne_3 = 'valeur 3'
//    WHERE condition
    return "update personnel set invite = ?, prenom = ?, nom = ?, photo = ?, service = ?, horaire = ?, id_vehicule = ? where id = ?";
}

QString StaffRepository::getDefaultInsertQuery() {
//    INSERT INTO table (nom_colonne_1, nom_colonne_2, ...
//    VALUES ('valeur 1', 'valeur 2', ...)
    return "insert into personnel (invite, prenom, nom, photo, service, horaire, id_vehicule) values (?, ?, ?, ?, ?, ?, ?) ";
}

void StaffRepository::tdelete(QString rowIndex) {
    BaseRepository::tdelete("personnel", rowIndex);
}

void StaffRepository::tupdate(QMap<QString, QString> data, QString id) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);
    query.prepare(getDefaultUpdateQuery());
    query.addBindValue(data["invite"]);
    query.addBindValue(data["prenom"]);
    query.addBindValue(data["nom"]);
    query.addBindValue(data["photo"]);
    query.addBindValue(data["service"]);
    query.addBindValue(data["horaire"]);
    query.addBindValue(data["id_vehicule"]);
    query.addBindValue(id);
    ConnectionPool::closeConnection(connection);
}

void StaffRepository::tinsert(QMap<QString, QString> data) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);
    query.prepare(getDefaultInsertQuery());
    query.addBindValue(data["invite"]);
    query.addBindValue(data["prenom"]);
    query.addBindValue(data["nom"]);
    query.addBindValue(data["photo"]);
    query.addBindValue(data["service"]);
    query.addBindValue(data["horaire"]);
    query.addBindValue(data["id_vehicule"]);
    ConnectionPool::closeConnection(connection);
}

void StaffRepository::tupdate(Entity &entity) {
    StaffEntity &e = dynamic_cast<StaffEntity &>(entity);
    QSqlDatabase connection = ConnectionPool::openConnection();

    QSqlQuery query(connection);
    query.prepare(getDefaultUpdateQuery());
    query.addBindValue(e.invite);
    query.addBindValue(e.prenom);
    query.addBindValue(e.nom);
    query.addBindValue(e.photo);
    query.addBindValue(e.service);
    query.addBindValue(e.horaires);
    query.addBindValue(e.id_vehicule);
    query.addBindValue(e.id);


    if (!query.exec()) {
        qDebug() << "Update enity car failed";
//        qDebug() << "Update enity " << entity.eName() << " failed";
    }
}

void StaffRepository::tinsert(Entity &entity) {
    StaffEntity &e = dynamic_cast<StaffEntity &>(entity);
    QSqlDatabase connection = ConnectionPool::openConnection();

    QSqlQuery query(connection);
    query.prepare(getDefaultInsertQuery());
    query.addBindValue(e.invite);
    query.addBindValue(e.prenom);
    query.addBindValue(e.nom);
    query.addBindValue(e.photo);
    query.addBindValue(e.service);
    query.addBindValue(e.horaires);
    query.addBindValue(e.id_vehicule);

    if (!query.exec()) {
        qDebug() << "Insert enity staff failed";
    }
}

QSqlQuery *StaffRepository::tselect() {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery *query = new QSqlQuery(connection);
    query->prepare("select * from personnel as p inner join vehicule as v  on p.id_vehicule = v.id");
    if (query->exec()) {
        ConnectionPool::closeConnection(connection);
        return query;
    }
    ConnectionPool::closeConnection(connection);
    return nullptr;

}

