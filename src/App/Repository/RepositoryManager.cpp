#include "include/RepositoryManager.h"
#include "include/CarRepository.h"
#include "include/StaffRepository.h"
#include "include/PlaceRepository.h"

RepositoryManager *RepositoryManager::instance = nullptr;

RepositoryManager &RepositoryManager::getInstance() {

    if (nullptr == instance) {
        instance = new RepositoryManager();
    }

    return *instance;
}

BaseRepository *RepositoryManager::getRepository(enum TABLES tables) {
    switch (tables){
        case CAR:
            return new CarRepository();
            break;
        case STAFF:
            return new StaffRepository();
            break;
        case PLACE:
            return new PlaceRepository();
            break;
    }
    return nullptr;
}

RepositoryManager::RepositoryManager() = default;
