//
// Created by vfractal on 10/05/2021.
//

#ifndef QTSECRETARY_BASEREPOSITORY_H
#define QTSECRETARY_BASEREPOSITORY_H


#include <QString>
#include <QSqlQuery>
#include <QMap>
#include "../../Entity/inlcude/Entity.h"

class BaseRepository {

public:
    virtual ~BaseRepository();

    virtual QSqlQuery *tselect(QString table);

    virtual void tdelete(QString rowIndex) = 0;

    virtual void tdelete(QString table, QString rowIndex);

    virtual void tupdate(QMap<QString, QString> data, QString id) = 0;

    virtual void tinsert(QMap<QString, QString> data) = 0;

    virtual void tupdate(Entity &entity) = 0;

    virtual void tinsert(Entity &entity) = 0;

    virtual QString getDefaultUpdateQuery() = 0;

    virtual QString getDefaultInsertQuery() = 0;
};


#endif //QTSECRETARY_BASEREPOSITORY_H
