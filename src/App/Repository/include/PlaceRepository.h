#ifndef QTSECRETARY_PLACEREPOSITORY_H
#define QTSECRETARY_PLACEREPOSITORY_H


#include "BaseRepository.h"

class PlaceRepository : public BaseRepository {

public:

    QSqlQuery *tselect();

    void tdelete(QString rowIndex) override;

    void tupdate(QMap<QString, QString> data, QString id) override;

    void tinsert(QMap<QString, QString> data) override;

    void tupdate( Entity &entity) override;

    void tinsert( Entity &entity) override;

    QString getDefaultUpdateQuery() override;

    QString getDefaultInsertQuery() override;
};


#endif //QTSECRETARY_PLACEREPOSITORY_H
