#ifndef QTSECRETARY_REPOSITORYMANAGER_H
#define QTSECRETARY_REPOSITORYMANAGER_H

#include "../utils/utils.h"

#include "BaseRepository.h"

class RepositoryManager {
public:
    static RepositoryManager &getInstance();
    BaseRepository* getRepository(enum TABLES tables);

private:
    RepositoryManager();
    static RepositoryManager* instance;
};


#endif //QTSECRETARY_REPOSITORYMANAGER_H
