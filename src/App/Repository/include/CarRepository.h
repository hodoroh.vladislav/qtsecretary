

#ifndef QTSECRETARY_CARREPOSITORY_H
#define QTSECRETARY_CARREPOSITORY_H


#include "BaseRepository.h"

class CarRepository : public BaseRepository {

public:
    QSqlQuery* tselect();

    void tdelete(QString rowIndex) override;

    void tupdate(QMap<QString, QString> data, QString id) override;

    void tinsert(QMap<QString, QString> data) override;

    void tupdate( Entity& enity) override;

    void tinsert( Entity& entity) override;

    QString getDefaultUpdateQuery() override;

    QString getDefaultInsertQuery() override;
};


#endif //QTSECRETARY_CARREPOSITORY_H
