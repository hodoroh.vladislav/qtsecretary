#include <QSqlDatabase>
#include "include/PlaceRepository.h"
#include "../../Core/include/ConnectionPool.h"
#include "../Entity/inlcude/PlaceEnity.h"

QString PlaceRepository::getDefaultUpdateQuery() {
    return "update place set libre = ?, numero = ? where id = ?";
}

QString PlaceRepository::getDefaultInsertQuery() {
    return "insert into place (libre, numero) values (?, ?)";
}

void PlaceRepository::tdelete(QString rowIndex) {
    BaseRepository::tdelete("place",rowIndex);
}

void PlaceRepository::tupdate(QMap<QString, QString> data, QString id) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);
    query.prepare(this->getDefaultUpdateQuery());
    query.addBindValue(data["libre"]);
    query.addBindValue(data["numero"]);
    query.addBindValue(id);
    ConnectionPool::closeConnection(connection);
}

void PlaceRepository::tinsert(QMap<QString, QString> data) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);

    query.prepare(this->getDefaultInsertQuery());
    query.addBindValue(data["immatr"]);
    query.addBindValue(data["type"]);
    query.addBindValue(data["color"]);

    ConnectionPool::closeConnection(connection);
}

void PlaceRepository::tupdate( Entity& entity) {
    PlaceEntity &e = dynamic_cast<PlaceEntity &>(entity);
    QSqlDatabase connection = ConnectionPool::openConnection();

    QSqlQuery query(connection);
    query.prepare(this->getDefaultUpdateQuery());
    query.addBindValue(e.isEmpty);
    query.addBindValue(e.numero);
    query.addBindValue(e.id);

    if (!query.exec()) {
        qDebug() << "Update enity car failed";
//        qDebug() << "Update enity " << entity.eName() << " failed";
    }
    ConnectionPool::closeConnection(connection);
}

void PlaceRepository::tinsert(Entity& entity) {
    PlaceEntity &e = dynamic_cast<PlaceEntity &>(entity);
    QSqlDatabase connection = ConnectionPool::openConnection();

    QSqlQuery query(connection);
    query.prepare(this->getDefaultInsertQuery());
    query.addBindValue(e.isEmpty);
    query.addBindValue(e.numero);
    if (!query.exec()) {
        qDebug() << "Update enity car failed";
//        qDebug() << "Update enity " << entity.eName() << " failed";
    }
    ConnectionPool::closeConnection(connection);
}

QSqlQuery *PlaceRepository::tselect() {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery *query = new QSqlQuery(connection);
    query->prepare("select * from place order by numero asc");
    if (query->exec()){
        ConnectionPool::closeConnection(connection);
        return query;
    }
    ConnectionPool::closeConnection(connection);
    return nullptr;}


