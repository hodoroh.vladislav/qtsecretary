#include <QSqlDatabase>
#include <QSqlQuery>

#include "include/BaseRepository.h"
#include "../../Core/include/ConnectionPool.h"


void BaseRepository::tdelete(QString table, QString index) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery query(connection);
    query.prepare("delete from " + table + " where id=" + index);

    if (query.exec())
        qDebug() << "Row deleted : index " << index;
    ConnectionPool::closeConnection(connection);
}

BaseRepository::~BaseRepository() {}

QSqlQuery *BaseRepository::tselect(QString table) {
    QSqlDatabase connection = ConnectionPool::openConnection();
    QSqlQuery *query = new QSqlQuery(connection);
    query->prepare("select * from " + table);
    if (query->exec()){
        ConnectionPool::closeConnection(connection);
        return query;
    }
    ConnectionPool::closeConnection(connection);
    return nullptr;
}
