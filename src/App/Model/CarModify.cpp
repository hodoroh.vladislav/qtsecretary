#include <QDebug>

#include "ui_carModify.h"
#include "include/CarModify.h"
#include "../../Core/include/ConnectionPool.h"
#include "../Repository/include/RepositoryManager.h"
#include <sstream>
#include <iostream>

CarModify::CarModify(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::CarModify) {
    ui->setupUi(this);
}

CarModify::~CarModify() {
    delete ui;
}

void CarModify::insert() {
    this->fetchData();
    auto *repo = RepositoryManager::getInstance().getRepository(CAR);
    repo->tinsert(this->entity);
    delete repo;
}


void CarModify::update() {
    auto *repo = RepositoryManager::getInstance().getRepository(CAR);
    repo->tupdate(this->entity);
    delete repo;

}

void CarModify::applyChangesOnModel(QStandardItemModel *model) {
    model->item(row, 1)->setText(entity.immatr.toString());
    model->item(row, 2)->setText(entity.type.toString());
    model->item(row, 3)->setText(entity.color.toString());

}

void CarModify::applyFromModel(QStandardItemModel *model, int row) {
    this->row = row;
    this->fetchDataFromModel(model);

    ui->line_immatr->setText(entity.immatr.toString());
    ui->line_type->setText(entity.type.toString());
    ui->line_color->setText(entity.color.toString());
}

void CarModify::fetchDataFromModel(QStandardItemModel *model) {
    entity.id = model->item(row, 0)->data(Qt::DisplayRole).toString();
    entity.immatr = model->item(row, 1)->data(Qt::DisplayRole).toString();
    entity.type = model->item(row, 2)->data(Qt::DisplayRole).toString();
    entity.color = model->item(row, 3)->data(Qt::DisplayRole).toString();
}

void CarModify::fetchData() {
    entity.immatr = ui->line_immatr->text();
    entity.type = ui->line_type->text();
    entity.color = ui->line_color->text();
}



