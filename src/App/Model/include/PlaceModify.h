#ifndef QTSECRETARY_PLACEMODIFY_H
#define QTSECRETARY_PLACEMODIFY_H


#include <QWidget>
#include "Editable.h"
#include "../../Entity/inlcude/PlaceEnity.h"


QT_BEGIN_NAMESPACE
namespace Ui { class PlaceModify; }
QT_END_NAMESPACE

class PlaceModify : QWidget, public Editable {
Q_OBJECT

public:

    explicit PlaceModify(QWidget *parent = 0);

    virtual ~PlaceModify();

    void update() override;

    void insert() override;

    void applyChangesOnModel(QStandardItemModel *model) override;

    void applyFromModel(QStandardItemModel *model, int row) override;

    void fetchDataFromModel(QStandardItemModel *model);

private slots:

    void onClickIsEmptyBnModify();

private:
    Ui::PlaceModify *ui = nullptr;
    int row;
    PlaceEntity entity;
    void fetchData();
};


#endif //QTSECRETARY_PLACEMODIFY_H
