

#ifndef QTSECRETARY_CARMODIFY_H
#define QTSECRETARY_CARMODIFY_H

#include <QWidget>
#include "Editable.h"
#include "../../Entity/inlcude/CarEntity.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CarModify; }
QT_END_NAMESPACE

class CarModify : QWidget, public Editable {
Q_OBJECT
public:
    explicit CarModify(QWidget *parent = 0);

    virtual ~CarModify();

    void update() override;

    void insert() override;

    void applyChangesOnModel(QStandardItemModel *model) override;

    void applyFromModel(QStandardItemModel *model, int row) override;

private slots:


private:
    Ui::CarModify *ui = nullptr;
    int row;
    CarEntity entity;

    void fetchDataFromModel(QStandardItemModel *model);

    void fetchData();
};


#endif //QTSECRETARY_CARMODIFY_H
