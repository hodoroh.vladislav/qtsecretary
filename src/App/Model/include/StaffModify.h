

#ifndef QTSECRETARY_STAFFMODIFY_H
#define QTSECRETARY_STAFFMODIFY_H


#include <QWidget>
#include "Editable.h"


QT_BEGIN_NAMESPACE
namespace Ui { class StaffModify; }
QT_END_NAMESPACE

class StaffModify : QWidget, public Editable {
Q_OBJECT


public:
    explicit StaffModify(QWidget *parent = 0);

    virtual ~StaffModify();

    void update() override;

    void insert() override;

    void applyChangesOnModel(QStandardItemModel *model) override;

    void applyFromModel(QStandardItemModel *model, int row) override;


private slots:

private:

    Ui::StaffModify *ui;

};


#endif //QTSECRETARY_STAFFMODIFY_H
