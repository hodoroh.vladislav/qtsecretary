

#ifndef QTSECRETARY_EDITABLE_H
#define QTSECRETARY_EDITABLE_H


#include <QSqlQuery>
#include <QStandardItemModel>

class Editable {
public:
    virtual ~Editable();

    virtual void update() = 0;

    virtual void insert() = 0;

    virtual void applyChangesOnModel(QStandardItemModel *model) = 0;

    virtual void applyFromModel(QStandardItemModel *model, int row) = 0;
};


#endif //QTSECRETARY_EDITABLE_H
