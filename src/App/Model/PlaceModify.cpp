#include <QStyle>
#include <QDebug>
#include <sstream>
#include "ui_placeModify.h"
#include "include/PlaceModify.h"
#include "../../Core/include/ConnectionPool.h"
#include "../Repository/include/PlaceRepository.h"
#include "../Repository/include/RepositoryManager.h"


PlaceModify::PlaceModify(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::PlaceModify) {
    ui->setupUi(this);
    qDebug() << "Created";

    QObject::connect(this->ui->bn_isEmpty, &QPushButton::clicked, this, [this]() {
        this->onClickIsEmptyBnModify();
    });
    QObject::connect(this->ui->spin_numero,
                     qOverload<int>(&QSpinBox::valueChanged),
                     this,
                     [this](int value) {
                         this->entity.numero = value;
                         qDebug() << this->entity.numero;
                     }
    );
}

PlaceModify::~PlaceModify() {
    delete ui;
}

void PlaceModify::update() {
    auto *repo = RepositoryManager::getInstance().getRepository(PLACE);
    repo->tupdate(this->entity);
    delete repo;
}

void PlaceModify::insert() {
    this->fetchData();

    auto *repo = RepositoryManager::getInstance().getRepository(PLACE);
    repo->tinsert(this->entity);
    delete repo;


}


static void setPlaceBg(QStandardItem *item, bool isEmpty) {
    QRadialGradient gradient(50, 50, 50, 50, 50);
    if (isEmpty) {
        gradient.setColorAt(0, QColor::fromRgbF(0, 1, 0, 0.85));
        gradient.setColorAt(1, QColor::fromRgbF(0, 1, 0, 0.50));
    } else {
        gradient.setColorAt(0, QColor::fromRgbF(1, 0, 0, 0.85));
        gradient.setColorAt(1, QColor::fromRgbF(1, 0, 0, 0.50));
    }

    QBrush brush(gradient);
    item->setBackground(brush);

}

void PlaceModify::applyChangesOnModel(QStandardItemModel *model) {
    QStandardItem *id = model->item(row, 0);
    QStandardItem *numero = model->item(row, 1);
    QStandardItem *libre = model->item(row, 2);


    qDebug() << QString(ui->spin_numero->value());
    numero->setText(this->entity.numero.toString());
    libre->setText(this->entity.isEmpty.toBool() ? "Libre" : "Occupé");
    setPlaceBg(libre, this->entity.isEmpty.toBool());
}


void PlaceModify::applyFromModel(QStandardItemModel *model, int row) {

    this->row = row;
    this->fetchDataFromModel(model);

    ui->spin_numero->setValue(this->entity.numero.toInt());
    ui->bn_isEmpty->setText(this->entity.isEmpty.toBool() ? "Libre" : "Occupé");
    ui->bn_isEmpty->setProperty("class", this->entity.isEmpty.toBool() ? "bn_success" : "bn_danger");
}

void PlaceModify::onClickIsEmptyBnModify() {
    this->entity.isEmpty = !this->entity.isEmpty.toBool();
    qDebug() << this->entity.isEmpty;
    ui->bn_isEmpty->setProperty("class", (this->entity.isEmpty.toBool() ? "bn_success" : "bn_danger"));
    ui->bn_isEmpty->setText((this->entity.isEmpty.toBool() ? "Libre" : "Occupé"));
    ui->bn_isEmpty->style()->unpolish(ui->bn_isEmpty);
    ui->bn_isEmpty->style()->polish(ui->bn_isEmpty);
    ui->bn_isEmpty->update();
}

void PlaceModify::fetchDataFromModel(QStandardItemModel *model) {

    this->entity.id = model->item(row, 0)->data(Qt::DisplayRole).toInt();
    this->entity.numero = model->item(row, 1)->data(Qt::DisplayRole).toInt();

    QString isEmpty = model->item(row, 2)->data(Qt::DisplayRole).toString();
    this->entity.isEmpty = isEmpty == "Libre";

}

void PlaceModify::fetchData() {
    this->entity.numero = ui->spin_numero->value();
    this->entity.isEmpty = ui->bn_isEmpty->property("class").toString() == "bn_success";
}




