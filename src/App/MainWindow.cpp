#include <QMessageBox>
#include <qmessagebox.h>
#include <QDebug>
#include <QScrollBar>
#include <QToolBar>
#include <QSqlField>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QMenu>

#include "include/MainWindow.h"
#include "ui_main.h"
#include "../Core/include/ConnectionPool.h"

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow) {
    ui->setupUi(this);
    //--------------------------------------------------------------------------------------
    // Clock part
    this->timer = new QTimer(this);
    QObject::connect(this->timer, SIGNAL(timeout()), this, SLOT(updateClock()));
    this->timer->start(1000 / 6);
    //--------------------------------------------------------------------------------------
    //  Right click menu config
    this->setupRightClick();//--------------------------------------------------------------------------------------
    // create mainController
    this->buildMainController();//--------------------------------------------------------------------------------------

    QObject::connect(ui->bn_submitModif, &QPushButton::clicked, this, [this]() {
        switch (this->action) {
            case INSERT:
                this->controller->insertData();
                //todo: after insert reconfigure model & data in order to get right data
                this->controller->show(this->controller->lastTable);
                break;
            case UPDATE:
                this->controller->updateData();
                break;
        }
        ui->bn_table->click();
    });
    //--------------------------------------------------------------------------------------

    this->mainMenuView = new MainMenuView(this->ui);
    this->confMainMenuLink();
    this->confTableBtn();
    //todo: rewrite
    this->ui->bn_home->click();
}

MainWindow::~MainWindow() {
    delete ui;
    delete mainMenuView;
}


void MainWindow::confMainMenuLink() {
    // get frame_bottom_west children, result is a list of QObject, but as there is only QPushButton & 2 QFrame
    QList<QObject *> list = this->ui->frame_bottom_west->children();
    qDebug() << list;
    //todo: rewrite
    // for each ellement in that list
    for (int i = 0; i < list.size(); i++) {
        // try to cast to QPushButton*
        qDebug() << "Ell\n" << list[i];
        auto *btn = qobject_cast<QPushButton *>(list[i]);
        if (btn) //if cast is successful, what mean that btn points to a QPushButton from frame_bottom_west frame
            // connect that button on action clicked and on action call this lambda,
            // lambda takes this & i copy (this is pointer so copy of this class, i as a new variable which stores i
            // value from for loop at current iteration),
            connect(btn, &QPushButton::clicked, this, [this, i]() {
                //call onMenuBtnClicked, i-1 cause in frame_bottom_west, we next order QWidget 3*QPushButton QWidget
                this->onMenuBtnClicked(i - 1);
            });
    }
}

void MainWindow::onMenuBtnClicked(int index) {
    auto *clickedButton = qobject_cast<QPushButton *>(sender());
    if (clickedButton) {
        if (this->ui->stackedWidget->currentIndex() != index) {
            this->mainMenuView->setBtnActive(clickedButton);
            this->ui->stackedWidget->setCurrentIndex(index);
        }
    }
}

void MainWindow::confTableBtn() {
    QObject::connect(this->ui->bn_car, &QPushButton::clicked, this, [&]() {
        this->ui->lab_tableName->setText("Table Vehicule");
        this->controller->show(CAR);
        this->ui->bn_table->click();  //todo:refactor
    });
    QObject::connect(this->ui->bn_staff, &QPushButton::clicked, this, [&]() {
        this->ui->lab_tableName->setText("Table Personnel");
        this->controller->show(STAFF);
        this->ui->bn_table->click();

    });
    QObject::connect(this->ui->bn_parking, &QPushButton::clicked, this, [&]() {
        this->ui->lab_tableName->setText("Table Place");
        this->controller->show(PLACE);
        this->ui->bn_table->click();
        // click manuel sur le button table ==> changer la page du stackWidget & mettre le button actif (changer son background)
    });
}

void MainWindow::customMenuRequested(QPoint pos) {
    //todo: solve modify page label issue;
    QAction *actionModify = new QAction("Modifier", this);
    connect(actionModify, &QAction::triggered, this, [this, pos]() {
        this->action = UPDATE;
        this->ui->bn_submitModif->setText("Modifier");
        this->controller->setUpdateRowView(pos);
        this->ui->bn_modify->click();

    });
    QAction *actionAdd = new QAction("Ajouter", this);
    connect(actionAdd, &QAction::triggered, this, [this, pos]() {
        this->action = INSERT;
        this->ui->bn_submitModif->setText("Ajouter");
        this->controller->setInsertRowView(pos);
        this->ui->bn_modify->click();
    });

    //todo: make a confirmation dialog;
    QAction *actionDelete = new QAction("Supprimer", this);
    connect(actionDelete, &QAction::triggered, this, [this, pos]() {
        this->controller->deleteRow(pos);
    });

    QMenu *menu = new QMenu(this);
    menu->setProperty("class","tableMenu");
    menu->addAction(actionAdd);
    menu->addAction(actionModify);
    menu->addAction(actionDelete);
    menu->setMinimumWidth(100);
    menu->popup(ui->tableView->viewport()->mapToGlobal(pos));
}


//todo: write in a separate file
void MainWindow::updateClock() {
    static int tindex = 0;
    //todo: rewrite for better performance
    QTime time = QTime::currentTime();
    QString date = QDate::currentDate().toString("dddd | dd MMM yyyy");
    QString clock_time = time.toString("hh | mm | ss");
    switch ((tindex %= 6)++) {
        case 0:
            clock_time[3] = '|';
            clock_time[8] = '|';
            break;
        case 1:
            clock_time[3] = '/';
            clock_time[8] = '/';
            break;
        case 2:
            clock_time[3] = '-';
            clock_time[8] = '-';
            break;
        case 3:
            clock_time[3] = '\\';
            clock_time[8] = '\\';
            break;
        case 4:
            clock_time[3] = '|';
            clock_time[8] = '|';
            break;
        case 5:
            clock_time[3] = ':';
            clock_time[8] = ':';
            break;
    }
    ui->lab_clock->setText(clock_time + " | " + date);
}

void MainWindow::setupRightClick() {
    QObject::connect(this->ui->tableView, SIGNAL(customContextMenuRequested(QPoint)), this,
                     SLOT(customMenuRequested(QPoint)));
    ui->tableView->setMouseTracking(true);
    ui->tableView->viewport()->setAttribute(Qt::WA_Hover, true);
    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);
}

void MainWindow::buildMainController() {
    this->controller = new Controller();
    this->controller->setModifyFrame(this->ui->modifyFrame);
    this->controller->setQtable(this->ui->tableView);
}

