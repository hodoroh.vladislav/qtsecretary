#include <iostream>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QString>

#include "include/dialoglogin.h"
#include "ui_dialoglogin.h"

//
DialogLogin::DialogLogin(QWidget *parent) :
        QDialog(parent),
        ui(new Ui::DialogLogin) {
    ui->setupUi(this);
}

DialogLogin::~DialogLogin() {
    delete ui;
}


void DialogLogin::on_buttonBox_accepted() {
    QString host = "127.0.0.1";
    QString lhost = "localhost";


    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "serveurmysql");
    db.setDatabaseName("parking");
    db.setPort(3306);
    db.setHostName(host);
//        db.setUserName(form->lineEditUser->text());
//        db.setPassword(form->lineEditPassword->text());
    db.setUserName("admin");
    db.setPassword("admin");


    if (!db.open()) {
        QMessageBox::critical(this, "Attention", "Pb d'accès", QMessageBox::Ok);

        exit(1);
    }

    qDebug() << "La base est connectée";

    for (auto &field : db.tables()) {
        qDebug() << field << "\n";
    }
    //     à compléter plus tard
    db.close();

    QSqlDatabase::removeDatabase("serveurmysql");


}














