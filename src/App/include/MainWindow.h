#ifndef QTSECRETARY_MAINWINDOW_H
#define QTSECRETARY_MAINWINDOW_H


#include <qt5/QtWidgets/QLineEdit>
#include <qt5/QtWidgets/QMainWindow>
#include <qt5/QtSql/QSqlTableModel>
#include <qt5/QtSql/qsqldatabase.h>
#include <QStackedWidget>
#include <QTableView>
#include "../View/include/MainMenuView.h"
#include "../View/include/View.h"
#include "../Controller/include/Controller.h"
#include "../utils/utils.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum ACTIONS{
    INSERT,UPDATE
};

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow() override;

private slots:

    void onMenuBtnClicked(int index);

    void customMenuRequested(QPoint point);

    void updateClock();


private:
    QTimer *timer;
    Ui::MainWindow *ui = nullptr;
    MainMenuView *mainMenuView = nullptr;
    enum ACTIONS action = UPDATE;
    Controller *controller = nullptr;

private:
    void confMainMenuLink();

    void confTableBtn();


    void setupRightClick();

    void buildMainController();
};


#endif //QTSECRETARY_MAINWINDOW_H
