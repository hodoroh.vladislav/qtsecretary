//#ifndef MAINWINDOW_H
//#define MAINWINDOW_H
//
//#include <qt5/QtWidgets/QLineEdit>
//#include <qt5/QtWidgets/QMainWindow>
//#include <qt5/QtSql/QSqlTableModel>
//#include <qt5/QtSql/qsqldatabase.h>
//#include <QStackedWidget>
//#include <QTableView>
//
//
//namespace Ui {
//    class MainWindow;
//}
//
//class MainWindow : public QMainWindow {
//Q_OBJECT
//
//
//public:
//    explicit MainWindow(QWidget *parent = 0);
//
//    ~MainWindow();
//
//private slots:
//
//    void paintEvent(QPaintEvent *);
//
//    void on_tableView_doubleClicked(const QModelIndex &index);
//
//    void on_actionSupprimer_triggered();
//
//    void on_actionModifier_triggered();
//
//    void on_actionInsertion_triggered();
//
//    void on_pushButton_clicked();
//
//    void on_homeButton_clicked();
//
//    void on_tableChoices_clicked();
//
//    void on_spotAccessButton_clicked();
//
//    void on_insert_button_clicked();
//
//    void onTableSelection(QString table);
//
//    void customMenuRequested(QPoint pos);
//
//    void customHeaderMenuRequested(QPoint pos);
//
//private:
//    Ui::MainWindow *ui;
//    QTableView *table = nullptr;
//    QSqlTableModel *tableModel = nullptr;
//
//    bool isSpotAvailable = false;
//    QStackedWidget *stack;
//    QString tableName = nullptr;
//
//    QSqlDatabase db;
//
//
//    void configureButton();
//
//    void UIHome();
//
//    void tableShow(QString table);
//
//    void configure();
//
//    void uiIsAvailablePlace();
//
//};
//
//#endif // MAINWINDOW_H
