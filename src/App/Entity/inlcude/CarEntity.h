

#ifndef QTSECRETARY_CAR_H
#define QTSECRETARY_CAR_H


#include <QVariant>
#include "Entity.h"

class CarEntity : public Entity {

public:
    ~CarEntity() override;
    QString table = "voiture";
    QVariant id;
    QVariant immatr;
    QVariant type;
    QVariant color;
};


#endif //QTSECRETARY_CAR_H
