

#ifndef QTSECRETARY_PLACEENITY_H
#define QTSECRETARY_PLACEENITY_H


#include <QVariant>
#include "Entity.h"

class PlaceEntity : public Entity {
public:
    ~PlaceEntity() override;
    QVariant id;
    QVariant isEmpty;
    QVariant numero;
};


#endif //QTSECRETARY_PLACEENITY_H
