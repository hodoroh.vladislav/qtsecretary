

#ifndef QTSECRETARY_STAFFENTITY_H
#define QTSECRETARY_STAFFENTITY_H


#include <QVariant>
#include "Entity.h"

class StaffEntity : public Entity {
public:
    QVariant id;
    QVariant invite;
    QVariant prenom;
    QVariant nom;
    QVariant photo;
    QVariant service;
    QVariant horaires;
    QVariant id_vehicule; //todo: ?? mb another solution?

    ~StaffEntity() override;
};


#endif //QTSECRETARY_STAFFENTITY_H
