
#include "include/StaffView.h"
#include "../Model/include/StaffModify.h"

void StaffView::show() {
    this->configQView();
    this->table->show();

}

StaffView::StaffView() = default;

void StaffView::configQView() {
    View::configQView();

    std::vector<const char*> headerOrder{"Id", "Invité", "Nom", "Prénom", "Service", "Horaires", "Vehicule", "Photo"};
    for (int i = 0; i < headerOrder.size(); i++) {
        model->setHeaderData(i, Qt::Horizontal, QObject::tr(headerOrder[i]));
    }


}

void setIsGuestBg(QStandardItem *item, bool isEmpty) {
    QRadialGradient gradient(50, 50, 50, 50, 50);
    if (isEmpty) {
        gradient.setColorAt(0, QColor::fromRgbF(0, 1, 0, 0.85));
        gradient.setColorAt(1, QColor::fromRgbF(0, 1, 0, 0));
    } else {
        gradient.setColorAt(0, QColor::fromRgbF(1, 0, 0, 0.85));
        gradient.setColorAt(1, QColor::fromRgbF(1, 0, 0, 0.50));
    }

    QBrush brush(gradient);
    item->setBackground(brush);

}


QStandardItemModel *StaffView::prepareModel(QSqlQuery &query) {
    QStandardItemModel *model = new QStandardItemModel();

    for (int index = 0; query.next(); index++) {
        bool isGuest = query.value("libre").toBool();
        QStandardItem *id = new QStandardItem(query.value("id").toString());
        QStandardItem *guest = new QStandardItem(isGuest ? "Invité" : "Personnel");
        QStandardItem *nom = new QStandardItem(query.value("nom").toString());
        QStandardItem *prenom = new QStandardItem(query.value("prenom").toString());
        QStandardItem *service = new QStandardItem(query.value("service").toString());
        QStandardItem *horaires = new QStandardItem(query.value("horaire").toString());
        QStandardItem *vimmatr = new QStandardItem(query.value("immatriculation").toString());
        QStandardItem *photo = new QStandardItem();


        QByteArray outByteArray = query.value("photo").toByteArray();
        QPixmap outPixmap = QPixmap::fromImage(QImage::fromData(outByteArray));

        photo->setIcon(outPixmap);

//        photo->setData(QVariant(outPixmap),Qt::DecorationRole);
//        outPixmap.scaled(320, 180, Qt::KeepAspectRatio, Qt::SmoothTransformation);
//        QStandardItem *photo = new QStandardItem(QIcon(outPixmap), query.value("nom").toString());

        setIsGuestBg(guest, isGuest);
        std::vector<QStandardItem *> order{id, guest, nom, prenom, service, horaires, vimmatr, photo};
        for (int i = 0; i < order.size(); i++) {
            model->setItem(index, i, order[i]);
        }
    }
    return model;
}

void StaffView::tmodify(int id) {
    this->prepareModifyView();
    if(id >= 0){
        this->editService->applyFromModel(this->model, id);
    }
}

void StaffView::prepareModifyView() {
    delete this->editService;
    this->editService = new StaffModify(this->modifyFrame);

}

StaffView::~StaffView() {
    delete editService;
}
