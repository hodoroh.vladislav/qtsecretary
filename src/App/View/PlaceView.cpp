#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include "include/PlaceView.h"
#include "../Model/include/PlaceModify.h"

void PlaceView::show() {
    this->configQView();

    table->show();
}

PlaceView::PlaceView() = default;

PlaceView::~PlaceView() {
    delete this->editService;

}

void PlaceView::configQView() {
    std::vector<const char *> headerOrder{"Id", "Numero", "Libre"};
    for (int i = 0; i < headerOrder.size(); i++) {
        model->setHeaderData(i, Qt::Horizontal, QObject::tr(headerOrder[i]));
    }
    View::configQView();

}

void setPlaceBg(QStandardItem *item, bool isEmpty) {
    QRadialGradient gradient(50, 50, 50, 50, 50);
    if (isEmpty) {
        gradient.setColorAt(0, QColor::fromRgbF(0, 1, 0, 0.85));
        gradient.setColorAt(1, QColor::fromRgbF(0, 1, 0, 0.50));
    } else {
        gradient.setColorAt(0, QColor::fromRgbF(1, 0, 0, 0.85));
        gradient.setColorAt(1, QColor::fromRgbF(1, 0, 0, 0.50));
    }

    QBrush brush(gradient);
    item->setBackground(brush);

}

QStandardItemModel *PlaceView::prepareModel(QSqlQuery &query) {
    auto *model = new QStandardItemModel();

    for (int index = 0; query.next(); index++) {
        auto *id = new QStandardItem(query.value("id").toString());
        auto *numero = new QStandardItem(query.value("numero").toString());
        bool isEmpty = query.value("libre").toBool();
        auto *libre = new QStandardItem(isEmpty ? "Libre" : "Occupé");

        setPlaceBg(libre, isEmpty);

        std::vector<QStandardItem *> order{id, numero, libre};
        for (int i = 0; i < order.size(); i++) {
            model->setItem(index, i, order[i]);
        }

    }
    return model;
}

void PlaceView::tmodify(int id) {
    this->prepareModifyView();

    if(id >= 0){
        this->editService->applyFromModel(this->model, id);
    }
}

void PlaceView::prepareModifyView() {
    delete this->editService;
    this->editService = new PlaceModify(this->modifyFrame);
}

