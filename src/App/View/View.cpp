#include "include/View.h"
#include "../../Core/include/ConnectionPool.h"

View::~View() {
    delete model;
}

QStandardItemModel *View::getModel() const {
    return model;
}

void View::setModel(QStandardItemModel *m) {
    if (this->model) {
        delete this->model;
        this->model = nullptr;
    }
    this->model = m;
}


void View::setTable(QTableView *table) {
    this->table = table;
}

void View::configQView() {
    this->table->reset();
    this->table->setModel(this->model);

    this->table->hideColumn(0);
    this->table->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    this->table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    qDebug() << this->table->model()->rowCount();
    for (int i = 0, rows = this->table->model()->rowCount(); i < rows; i++) {
        table->setRowHeight(i, 80);
    }
}

void View::tdelete(int rowIndex) {
    QString recordId = this->model->item(rowIndex, 0)->data(Qt::DisplayRole).toString();
    model->removeRow(rowIndex);
}

QFrame *View::getModifyFrame() const {
    return modifyFrame;
}

void View::setModifyFrame(QFrame *modifyFrame) {
    View::modifyFrame = modifyFrame;
}

void View::prepareEditView() {
    this->editService->applyChangesOnModel(this->model);
}


Editable *View::getEditService() const {
    return editService;
}

void View::setEditService(Editable *editService) {
    View::editService = editService;
}


