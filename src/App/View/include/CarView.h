

#ifndef QTSECRETARY_CARVIEW_H
#define QTSECRETARY_CARVIEW_H

#include "View.h"

class CarView : public View {

public:
    CarView();
    ~CarView() override;

    void show() override;

    void configQView() override;

    void tmodify(int id) override;

    void prepareModifyView() override;

    QStandardItemModel *prepareModel(QSqlQuery &query) override;

};


#endif //QTSECRETARY_CARVIEW_H
