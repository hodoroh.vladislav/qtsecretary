#ifndef QTSECRETARY_STAFFVIEW_H
#define QTSECRETARY_STAFFVIEW_H

#include "View.h"

class StaffView : public View {

public:
    StaffView();
    virtual ~StaffView() override;

    void show() override;

    void configQView() override;

    void tmodify(int id) override;

    void prepareModifyView() override;

    QStandardItemModel *prepareModel(QSqlQuery &query) override;

};


#endif //QTSECRETARY_STAFFVIEW_H
