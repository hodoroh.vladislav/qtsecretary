

#ifndef QTSECRETARY_MAINMENUVIEW_H
#define QTSECRETARY_MAINMENUVIEW_H

#include <ui_main.h>

class MainMenuView {

public:
    Ui::MainWindow *ui;

    MainMenuView(Ui::MainWindow *ui);

    ~MainMenuView();

    void stupDefaultView();

    void setBtnActive(QPushButton *btn);


};


#endif //QTSECRETARY_MAINMENUVIEW_H
