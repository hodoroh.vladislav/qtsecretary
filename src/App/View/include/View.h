#ifndef QTSECRETARY_VIEW_H
#define QTSECRETARY_VIEW_H


#include <QTableView>
#include <QString>
#include <QDebug>
#include <QStandardItemModel>
#include <QSqlQuery>
#include "../../Repository/include/BaseRepository.h"
#include "../../Model/include/Editable.h"


class View {
public:
    QStandardItemModel *model = nullptr;
    QTableView *table = nullptr;
    //------------------------------
    QFrame *modifyFrame = nullptr;
    Editable *editService = nullptr;

    virtual ~View();

    QFrame *getModifyFrame() const;

    void setModifyFrame(QFrame *modifyFrame);

    QTableView *getTable() const;

    void setTable(QTableView *table);

    QStandardItemModel *getModel() const;

    void setModel(QStandardItemModel *model);

    Editable *getEditService() const;

    void setEditService(Editable *editService);

    virtual void show() = 0;

    virtual void configQView();

    virtual QStandardItemModel *prepareModel(QSqlQuery &query) = 0;

    virtual void prepareModifyView() = 0;

    void tdelete(int id);

    virtual void tmodify(int id) = 0;

    void prepareEditView();
};


#endif //QTSECRETARY_VIEW_H
