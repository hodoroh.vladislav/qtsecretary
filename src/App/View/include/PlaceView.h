#ifndef QTSECRETARY_PLACEVIEW_H
#define QTSECRETARY_PLACEVIEW_H

#include "View.h"

class PlaceView : public View {

public:
    PlaceView();
    ~PlaceView() override;

    void show() override;

    void configQView() override;

    void tmodify(int id) override;

    void prepareModifyView();

    QStandardItemModel *prepareModel(QSqlQuery &query) override;

};


#endif //QTSECRETARY_PLACEVIEW_H
