

#include "include/CarView.h"
#include "../Model/include/CarModify.h"

void CarView::show() {
    this->configQView();
}

CarView::CarView() = default;

CarView::~CarView() {
    delete editService;
}

void CarView::configQView() {
    View::configQView();

    std::vector<const char *> headerOrder{"Id", "Immatriculation", "Type", "Couleur"};
    for (int i = 0; i < headerOrder.size(); i++) {
        model->setHeaderData(i, Qt::Horizontal, QObject::tr(headerOrder[i]));
    }

}

QStandardItemModel *CarView::prepareModel(QSqlQuery &query) {
    auto *model = new QStandardItemModel();

    for (int index = 0; query.next(); index++) {
        auto *id = new QStandardItem(query.value("id").toString());
        auto *immatriculation = new QStandardItem(query.value("immatriculation").toString());
        auto *type = new QStandardItem(query.value("type").toString());
        auto *couleur = new QStandardItem(query.value("couleur").toString());

        std::vector<QStandardItem *> order{id, immatriculation, type, couleur};
        for (int i = 0; i < order.size(); i++) {
            model->setItem(index, i, order[i]);
        }
    }
    return model;
}

void CarView::tmodify(int id) {
    this->prepareModifyView();
    if (id >= 0) {
        this->editService->applyFromModel(this->model, id);
    }

}

void CarView::prepareModifyView() {
    delete this->editService;
    this->editService = new CarModify(this->modifyFrame);
}
