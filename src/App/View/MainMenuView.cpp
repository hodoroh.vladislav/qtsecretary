#include <QObject>
#include <utility>
#include <QDebug>
#include <qt5/QtWidgets/QMainWindow>
#include "include/MainMenuView.h"

MainMenuView::MainMenuView(Ui::MainWindow *ui) : ui(ui) {
    this->stupDefaultView();
}

void MainMenuView::stupDefaultView() {
    QList<QObject *> list = this->ui->frame_bottom_west->children();
//    qDebug() << list;
    for (auto &ellem : list) {
        QPushButton *btn = qobject_cast<QPushButton *>(ellem);
        if (btn){
            btn->setProperty("btn_active", false);
            btn->style()->unpolish(btn);
            btn->style()->polish(btn);
            btn->update();
        }

    }

}

MainMenuView::~MainMenuView() {

}

void MainMenuView::setBtnActive(QPushButton *btn) {
    this->stupDefaultView();
    btn->setProperty("btn_active", true);
    btn->style()->unpolish(btn);
    btn->style()->polish(btn);
    btn->update();

//    qDebug() << btn->property("btn_active");
}
