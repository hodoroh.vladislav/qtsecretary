

#include "include/CarController.h"
#include "../Repository/include/RepositoryManager.h"
#include "../Repository/include/CarRepository.h"
#include "../View/include/CarView.h"

CarController::CarController() {
    this->view = new CarView();
}

CarController::~CarController() {

}

void CarController::configureView() {
    //        this->lastTable = tables;
    this->view->setModifyFrame(modifyFrame);
    QStandardItemModel *model = createModel();
    if (!model) {
        qDebug() << "Error moddel";
        return;
    }
    this->view->setTable(this->table);
    this->view->setModel(model);

}

QStandardItemModel *CarController::createModel() {
    auto *repo = (CarRepository *) RepositoryManager::getInstance().getRepository(CAR);

    QSqlQuery *query = repo->tselect();
    if (query->exec()) {
        delete repo;
        return this->view->prepareModel(*query);
    }
    return nullptr;

}


void CarController::updateView() {
    qDebug() << "Pres modify, update";
    this->view->getEditService()->update();
    this->view->prepareEditView();
}

void CarController::insertView() {
    this->view->getEditService()->insert();
    this->view->prepareEditView();
}

void CarController::show() {
    this->configureView();
    this->view->show();
}


QTableView *CarController::getTable() const {
    return table;
}

void CarController::setTable(QTableView *table) {
    CarController::table = table;
}

QFrame *CarController::getModifyFrame() const {
    return modifyFrame;
}

void CarController::setModifyFrame(QFrame *modifyFrame) {
    CarController::modifyFrame = modifyFrame;
}

void CarController::deleteRow(int index) {
    QString recordId = this->view->getModel()->item(index, 0)->data(Qt::DisplayRole).toString();

    auto *repo = RepositoryManager::getInstance().getRepository(CAR);
    repo->tdelete(recordId);
    delete repo;

}



