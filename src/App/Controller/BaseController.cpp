

#include "include/BaseController.h"

BaseController::~BaseController() {
    delete view;
}

View *BaseController::getView() const {
    return view;
}

void BaseController::setView(View *view) {
    BaseController::view = view;
}
