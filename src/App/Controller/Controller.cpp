#include "include/Controller.h"
#include "../../Core/include/ConnectionPool.h"

#include "include/CarController.h"
#include "include/PlaceController.h"
#include "include/StaffController.h"

Controller::~Controller() {
    delete this->actualController;
}


void Controller::show(enum TABLES table) {
    this->setupController(table);
    this->actualController->show();
}

void Controller::deleteRow(QPoint pos) {
    QModelIndex index = this->table->indexAt(pos);
    View *view = this->actualController->getView();
    if (index.row() >= 0 && view) {
        qDebug() << "Deleting row " << index;
        view->tdelete(index.row());
        this->actualController->deleteRow(index.row());

    }
}

void Controller::setUpdateRowView(QPoint point) {
    QModelIndex index = this->table->indexAt(point);
    View *view = this->actualController->getView();
    qDebug() << "Row index" << index;
    if (view && index.row() >= 0) {
        view->tmodify(index.row());
        qDebug() << "Modifying row" << index;
    }
}

void Controller::setInsertRowView(QPoint point) {
    View *view = this->actualController->getView();
    if (view)
        view->tmodify(-1);
}

void Controller::insertData() {
    View *view = this->actualController->getView();
    if (view) {
        view->getEditService()->insert();
        view->prepareEditView();
    }
}

void Controller::updateData() {
    View *view = this->actualController->getView();
    if (view) {
        view->getEditService()->update();
        view->prepareEditView();

    }
}

void Controller::setupController(enum TABLES table) {
    delete this->actualController;
    try{
        switch (table) {
            case CAR: {
                auto *controller = new CarController;
                controller->setModifyFrame(this->modifyFrame);
                controller->setTable(this->table);
                this->actualController = controller;
            }
                break;
            case PLACE: {
                auto *controller = new PlaceController;
                controller->setModifyFrame(this->modifyFrame);
                controller->setTable(this->table);
                this->actualController = controller;
            }
                break;
            case STAFF: {
                auto *controller = new StaffController;
                controller->setModifyFrame(this->modifyFrame);
                controller->setTable(this->table);
                this->actualController = controller;
            }
                break;
            case NONE:
                this->actualController = nullptr;
                break;
        }

    }catch(...){
        qDebug() << "Error controller creation";
        QCoreApplication::exit();
    }
    this->lastTable = table;

}

void Controller::setModifyFrame(QFrame *modifyFrame) {
    this->modifyFrame = modifyFrame;
}


void Controller::setQtable(QTableView *qtable) {
    this->table = qtable;
}


