

#include "include/StaffController.h"
#include "../View/include/StaffView.h"
#include "../../Core/include/ConnectionPool.h"
#include "../Repository/include/StaffRepository.h"

void StaffController::show() {
    this->configureView();
    this->view->show();
}

StaffController::StaffController() {
    this->view = new StaffView();
}

StaffController::~StaffController() {

}

void StaffController::configureView() {
    this->view->setModifyFrame(modifyFrame);
    QStandardItemModel *model = this->createModel();
    if (!model) {
        qDebug() << "Error moddel";
        return;
    }
    this->view->setTable(this->table);
    this->view->setModel(model);
}

QStandardItemModel *StaffController::createModel() {
    std::unique_ptr<BaseRepository *> repo = std::make_unique<BaseRepository *>(RepositoryManager::getInstance().getRepository(STAFF));

    auto * r = (StaffRepository *) repo.get();

    QSqlQuery *query = r->tselect();
    if (query->exec()) {
        return this->view->prepareModel(*query);
    }
    return nullptr;

}



void StaffController::updateView() {
    qDebug() << "Pres modify, update";
    this->view->getEditService()->update();
    this->view->prepareEditView();
}

void StaffController::insertView() {
    this->view->getEditService()->insert();
    this->view->prepareEditView();
}

QTableView *StaffController::getTable() const {
    return table;
}

void StaffController::setTable(QTableView *table) {
    StaffController::table = table;
}

QFrame *StaffController::getModifyFrame() const {
    return modifyFrame;
}

void StaffController::setModifyFrame(QFrame *modifyFrame) {
    StaffController::modifyFrame = modifyFrame;
}

void StaffController::deleteRow(int index) {
    QString recordId = this->view->getModel()->item(index,0)->data(Qt::DisplayRole).toString();

    auto *repo = RepositoryManager::getInstance().getRepository(STAFF);
    repo->tdelete(recordId);
    delete repo;
}

