

#include "include/PlaceController.h"
#include "../Repository/include/RepositoryManager.h"
#include "../Repository/include/PlaceRepository.h"
#include "../View/include/PlaceView.h"


PlaceController::PlaceController() {
    this->view = new PlaceView();
}

PlaceController::~PlaceController() {
}


void PlaceController::configureView() {
    //        this->lastTable = tables;
    this->view->setModifyFrame(modifyFrame);
    QStandardItemModel *model = createModel();
    if (!model) {
        qDebug() << "Error moddel";
        return;
    }
    this->view->setTable(this->table);
    this->view->setModel(model);

}

QStandardItemModel *PlaceController::createModel() {
    std::unique_ptr<BaseRepository *> repo = std::make_unique<BaseRepository *>(
            RepositoryManager::getInstance().getRepository(STAFF));

    auto *r = (PlaceRepository *) repo.get();

    QSqlQuery *query = r->tselect();
    if (query->exec()) {
        return this->view->prepareModel(*query);
    }
    return nullptr;

}


void PlaceController::updateView() {
    qDebug() << "Pres modify, update";
    this->view->getEditService()->update();
    this->view->prepareEditView();
}

void PlaceController::insertView() {
    this->view->getEditService()->insert();
    this->view->prepareEditView();
}

void PlaceController::show() {
    this->configureView();
    this->view->show();
}


QTableView *PlaceController::getTable() const {
    return table;
}

void PlaceController::setTable(QTableView *table) {
    PlaceController::table = table;
}

QFrame *PlaceController::getModifyFrame() const {
    return modifyFrame;
}

void PlaceController::setModifyFrame(QFrame *modifyFrame) {
    PlaceController::modifyFrame = modifyFrame;
}

void PlaceController::deleteRow(int index) {
    QString recordId = this->view->getModel()->item(index,0)->data(Qt::DisplayRole).toString();

    auto *repo = RepositoryManager::getInstance().getRepository(PLACE);
    repo->tdelete(recordId);
    delete repo;
}


