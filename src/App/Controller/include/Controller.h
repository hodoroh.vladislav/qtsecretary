

#ifndef QTSECRETARY_CONTROLLER_H
#define QTSECRETARY_CONTROLLER_H

#include <QStandardItemModel>
#include <QSqlTableModel>


#include "../../View/include/View.h"
#include "../../utils/utils.h"
#include "BaseController.h"
/**
 * @brief controller
 */
class Controller {

public:

    QTableView *table = nullptr;
    QFrame *modifyFrame = nullptr;
    BaseController* actualController = nullptr;
    enum TABLES lastTable = NONE;

    ~Controller();

    void show(enum TABLES);

    void setupController(enum TABLES);

    void deleteRow(QPoint point);

    void setQtable(QTableView *qtable);

    void setModifyFrame(QFrame *modifyFrame);


    void setUpdateRowView(QPoint point);

    void setInsertRowView(QPoint point);

    void insertData();

    void updateData();
};


#endif //QTSECRETARY_CONTROLLER_H
