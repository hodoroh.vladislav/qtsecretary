#ifndef QTSECRETARY_BASECONTROLLER_H
#define QTSECRETARY_BASECONTROLLER_H


#include "../../View/include/View.h"

class BaseController {

public:

    View *view;

    virtual ~BaseController();

    virtual void show() = 0;

    virtual void updateView() = 0;

    virtual void insertView() = 0;

    virtual void deleteRow(int index) = 0;

    View *getView() const;

    void setView(View *view);

};


#endif //QTSECRETARY_BASECONTROLLER_H
