

#ifndef QTSECRETARY_CARCONTROLLER_H
#define QTSECRETARY_CARCONTROLLER_H


#include "BaseController.h"

class CarController : public BaseController {
    QTableView *table;
    QFrame *modifyFrame;

public:
    CarController();

    virtual ~CarController();

    void show() override;

    void updateView() override;

    void insertView() override;

    void deleteRow(int index) override;


    void configureView();

    QStandardItemModel *createModel();

    QTableView *getTable() const;

    void setTable(QTableView *table);

    QFrame *getModifyFrame() const;

    void setModifyFrame(QFrame *modifyFrame);

};


#endif //QTSECRETARY_CARCONTROLLER_H
