

#ifndef QTSECRETARY_STAFFCONTROLLER_H
#define QTSECRETARY_STAFFCONTROLLER_H


#include <QTableView>
#include <QFrame>
#include "BaseController.h"
#include "../../View/include/View.h"
#include "../../Repository/include/RepositoryManager.h"

class StaffController : public BaseController {

    QTableView *table;
    QFrame *modifyFrame;


    QStandardItemModel *createModel();

    void configureView();

public:
    StaffController();

    ~StaffController();

    void show() override;

    virtual void updateView();

    virtual void insertView();

    void deleteRow(int index) override;

    QTableView *getTable() const;

    void setTable(QTableView *table);

    QFrame *getModifyFrame() const;

    void setModifyFrame(QFrame *modifyFrame);

};


#endif //QTSECRETARY_STAFFCONTROLLER_H
