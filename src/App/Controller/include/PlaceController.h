

#ifndef QTSECRETARY_PLACECONTROLLER_H
#define QTSECRETARY_PLACECONTROLLER_H


#include "BaseController.h"

class PlaceController : public BaseController {

    QTableView *table;
    QFrame *modifyFrame;

public:
    PlaceController();

    virtual ~PlaceController();

    void show() override;

    void updateView() override;

    void insertView() override;

    void deleteRow(int index) override;

    QStandardItemModel *createModel();
    void configureView();

    QTableView *getTable() const;

    void setTable(QTableView *table);

    QFrame *getModifyFrame() const;

    void setModifyFrame(QFrame *modifyFrame);
};


#endif //QTSECRETARY_PLACECONTROLLER_H
