#ifndef CONNECTIONPOOL_H
#define CONNECTIONPOOL_H

#include <QtSql>
#include <QQueue>
#include <QString>
#include <QMutex>
#include <QMutexLocker>


class ConnectionPool {
public:
    static void release(); // Close all database connections
    static QSqlDatabase openConnection();                 // Get database connection
    static void closeConnection(QSqlDatabase connection); // Release the database connection back to the connection pool
    ~ConnectionPool();

private:
    static ConnectionPool &getInstance();

    ConnectionPool();

    ConnectionPool(const ConnectionPool &other);

    ConnectionPool &operator=(const ConnectionPool &other);

    QSqlDatabase createConnection(const QString &connectionName); // Create a database connection
    QQueue<QString> usedConnectionNames;   // Used database connection name
    QQueue<QString> unusedConnectionNames; // Unused database connection name
    // database information
    QString hostName;
    QString databaseName;
    QString username;
    QString password;
    QString databaseType;

    bool testOnBorrow;    // Verify the connection is valid when getting the connection
    QString testOnBorrowSql; // Test the SQL to access the database
    int maxWaitTime;  // Get the maximum waiting time for connection
    int waitInterval; // wait interval time when trying to get a connection
    int maxConnectionCount; // Maximum number of connections
    static QMutex mutex;
    static QWaitCondition waitConnection;
    static ConnectionPool *instance;
};

#endif // CONNECTIONPOOL_H