#include "include/ConnectionPool.h"
#include <QDebug>

QMutex ConnectionPool::mutex;
QWaitCondition ConnectionPool::waitConnection;
ConnectionPool *ConnectionPool::instance = NULL;

ConnectionPool::ConnectionPool() {
    // The information to create a database connection needs to be obtained by reading the configuration file during actual development,
    //todo: refactor this;
    hostName = "127.0.0.1";
    databaseName = "parking";
    username = "admin";
    password = "admin";
    databaseType = "QMYSQL";
    testOnBorrow = true;
    testOnBorrowSql = "SELECT 1";

    maxWaitTime = 1000;
    waitInterval = 200;
    maxConnectionCount = 5;
}

ConnectionPool::~ConnectionPool() {
    // Delete all connections when destroying the connection pool
            foreach(QString connectionName, usedConnectionNames) {
            QSqlDatabase::removeDatabase(connectionName);
        }
            foreach(QString connectionName, unusedConnectionNames) {
            QSqlDatabase::removeDatabase(connectionName);
        }
}

ConnectionPool &ConnectionPool::getInstance() {
    if (nullptr == instance) {
        QMutexLocker locker(&mutex);

        if (nullptr == instance) {
            instance = new ConnectionPool();
        }
    }

    return *instance;
}

void ConnectionPool::release() {
    QMutexLocker locker(&mutex);
    delete instance;
    instance = nullptr;
}

QSqlDatabase ConnectionPool::openConnection() {
    ConnectionPool &pool = ConnectionPool::getInstance();
    QString connectionName;

    QMutexLocker locker(&mutex);

    // Number of connections created
    int connectionCount = pool.unusedConnectionNames.size() + pool.usedConnectionNames.size();

    // If the connection has been used up, wait for waitInterval milliseconds to see if there is an available connection, the longest wait is maxWaitTime milliseconds
    for (int i = 0;
         i < pool.maxWaitTime
         && pool.unusedConnectionNames.empty() && connectionCount == pool.maxConnectionCount;
         i += pool.waitInterval) {
        waitConnection.wait(&mutex, pool.waitInterval);

        // Recalculate the number of connections created
        connectionCount = pool.unusedConnectionNames.size() + pool.usedConnectionNames.size();
    }

    if (!pool.unusedConnectionNames.empty()) {
        // There are already recovered connections, reuse them
        connectionName = pool.unusedConnectionNames.dequeue();
        qDebug() << "Unused created connection";
    } else if (connectionCount < pool.maxConnectionCount) {
        // No connection has been recovered, but the maximum number of connections is not reached, a new connection is created
        connectionName = QString("Connection-%1").arg(connectionCount + 1);
    } else {
        // The maximum number of connections has been reached
        qDebug() << "Cannot create more connections.";
        return QSqlDatabase();
    }

    // Create connection
    QSqlDatabase db = pool.createConnection(connectionName);

    // Only valid connections are put into usedConnectionNames
    if (db.isOpen()) {
        pool.usedConnectionNames.enqueue(connectionName);
    }

    return db;
}

void ConnectionPool::closeConnection(QSqlDatabase connection) {
    ConnectionPool &pool = ConnectionPool::getInstance();
    QString connectionName = connection.connectionName();

    // If it is the connection we created, delete it from used and put it in unused
    if (pool.usedConnectionNames.contains(connectionName)) {
        QMutexLocker locker(&mutex);
        pool.usedConnectionNames.removeOne(connectionName);
        pool.unusedConnectionNames.enqueue(connectionName);
        waitConnection.wakeOne();
    }
}

QSqlDatabase ConnectionPool::createConnection(const QString &connectionName) {
    // The connection has already been created, reuse it instead of recreating it
    if (QSqlDatabase::contains(connectionName)) {
        QSqlDatabase db1 = QSqlDatabase::database(connectionName);

        if (testOnBorrow) {
            // Access the database before returning to the connection, if the connection is broken, re-establish the connection
            qDebug() << "Test connection on borrow, execute:" << testOnBorrowSql << ", for" << connectionName;
            QSqlQuery query(testOnBorrowSql, db1);

            if (query.lastError().type() != QSqlError::NoError && !db1.open()) {
                qDebug() << "Open datatabase error:" << db1.lastError().text();
                return QSqlDatabase();
            }
        }

        return db1;
    }

    // Create a new connection
    QSqlDatabase db = QSqlDatabase::addDatabase(databaseType, connectionName);
    db.setHostName(hostName);
    db.setDatabaseName(databaseName);
    db.setUserName(username);
    db.setPassword(password);

    if (!db.open()) {
        qDebug() << "Open datatabase error:" << db.lastError().text();
        return QSqlDatabase();
    }

    return db;
}